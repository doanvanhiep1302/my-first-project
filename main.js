
var userApi = "http://localhost:3000/user";

function start() {
  getTasks(renderTasks);

  addForm();
}

start();

var list = document.querySelector("ul");
// list.addEventListener(
//   "click",
//   function (ev) {
//     if (ev.target.tagName === "H3") {
//       ev.target.classList.toggle("checked");
//     }
//   },
//   false
// );
// var close = document.getElementsByClassName("close");
// var i;
// for (i = 0; i < close.length; i++){
//   close[i].onclick = function(){
//       var div = this.parentElement;
//       div.style.display ="none";
//   }
// }

function getTasks(callback) {
  fetch(userApi)
    .then(function (response) {
      return response.json();
    })
    .then(callback);
}

function createTasks(data, callback) {
  var options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    redirect: "follow",
    body: JSON.stringify(data),
  };
  fetch(userApi, options)
    .then(function (response) {
      response.json();
    })
    .then(callback);
}



function deleteTask(id) {
  console.log(id)
  var s = confirm("Hãy suy nghĩ kĩ trước khi xóa !");
  if (s) {
    var options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(userApi + "/" + id, options)
      .then(function (response) {
        response.json();
      })
      .then(function () {
          getTasks(renderTasks);
      });
  }
}


function renderTasks(tasks) {
 
  var tasksList = document.querySelector("#user-list");
  var htmls = tasks.map(function (task) {
  
    return`
      <li>
        <h4><i class="fa fa-file"></i> ${task.name}</h4>
        <button class="close" type="button" onclick="deleteTask(${task.id})">Xoá</button>
        
      </li>

    `
  });
  tasksList.innerHTML = htmls.join("");

}

function addForm() {
  var createBtn = document.querySelector("#saveBtn");
  createBtn.onclick = function () {
    var name = document.querySelector('input[name="username"]').value;

    var formData = {
      name: name,
    };

    if (name != "") {
      alert("OK được rồi !");
      createTasks(formData);
    } else {
      alert("Chả có gì cả. Nhập lại đi !");
    }
  };
}

